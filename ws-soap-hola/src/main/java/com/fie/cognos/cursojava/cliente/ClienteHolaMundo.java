package com.fie.cognos.cursojava.cliente;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

public class ClienteHolaMundo {
	public static void main(String[] args) {
		HolaMundoServiceService service = new HolaMundoServiceService();
		HolaMundoService cliente = service.getHolaMundoServicePort();
		Map<String, Object> mapRequests = ((BindingProvider) cliente).getRequestContext();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		headers.put("usuario", Collections.singletonList("ariel"));
		headers.put("password", Collections.singletonList("123"));
		mapRequests.put(MessageContext.HTTP_REQUEST_HEADERS,headers);
		
		String saludo;
		try {
			saludo = cliente.saludar();
			System.out.println(saludo);
		} catch (Exception_Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		//Persona persona;
		
		//persona = cliente.obtenerPersona("Juan", "Perez", "Perez", 25);
		
		//System.out.println(persona.nombre + " " + persona.paterno + " " + persona.materno + " " + persona.edad);
		
		/*List<Persona> personas = cliente.listarPeronas();
		
		for(Persona personaItem: personas)
		{
			System.out.println(personaItem.nombre);
		}*/
	}
}
