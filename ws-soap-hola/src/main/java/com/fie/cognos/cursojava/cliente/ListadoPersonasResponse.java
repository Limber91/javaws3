
package com.fie.cognos.cursojava.cliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para listadoPersonasResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="listadoPersonasResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listadoPersonas" type="{http://services.cursojava.cognos.fie.com/}persona" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listadoPersonasResponse", propOrder = {
    "listadoPersonas"
})
public class ListadoPersonasResponse {

    protected List<Persona> listadoPersonas;

    /**
     * Gets the value of the listadoPersonas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listadoPersonas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListadoPersonas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Persona }
     * 
     * 
     */
    public List<Persona> getListadoPersonas() {
        if (listadoPersonas == null) {
            listadoPersonas = new ArrayList<Persona>();
        }
        return this.listadoPersonas;
    }

}
