package com.fie.cognos.cursojava.entidades;

public class Persona {

	private String nombre;
	private String paterno;
	private String materno;
	private int edad;
	
	public Persona(String nom, String pat, String mat, int ed) {
		this.setNombre(nom);
		this.setPaterno(pat);
		this.setMaterno(mat);
		this.setEdad(ed);
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
}
