package com.fie.cognos.cursojava.services;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.fie.cognos.cursojava.entidades.Persona;

@WebService
@HandlerChain(file = "handler-chain.xml")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class HolaMundoService {
	
	@Resource
	WebServiceContext wsc;
	
	@WebMethod
	public String saludar() throws Exception {
		MessageContext messageContext = wsc.getMessageContext();
		Map<String,Object> headers = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
		
		List<String> usuario = (List<String>)headers.get("usuario");
		List<String> password = (List<String>)headers.get("password");
		
		if(usuario != null && password != null) {
			if(usuario.get(0).equals("limber") && password.get(0).equals("patito")) {
				return "Bienvenido Limber";
			}
		}
		//return "Error de autenticacion.";
		
		throw new Exception("Error el usuario no es correcto");
	}
	
	@WebMethod
	@WebResult(name = "persona")
	public Persona obtenerPersona(@WebParam(name = "nombre")String nombre, @WebParam(name = "paterno") String paterno, @WebParam(name = "materno") String materno, @WebParam(name = "edad") int edad) {
		return new Persona(nombre, paterno, materno, edad);
	}
	
	@WebMethod
	@WebResult(name = "listadoPersonas")
	public ArrayList<Persona> listadoPersonas() {
		
		Persona p1 = new Persona("nombre1", "paterno1", "materno1", 1);
		Persona p2 = new Persona("nombre2", "paterno2", "materno2", 2);
		Persona p3 = new Persona("nombre3", "paterno3", "materno3", 3);
		Persona p4 = new Persona("nombre4", "paterno4", "materno4", 4);
		Persona p5 = new Persona("nombre5", "paterno5", "materno5", 5);
		
		ArrayList<Persona> listaP= new ArrayList<Persona>();
		listaP.add(p1);
		listaP.add(p2);
		listaP.add(p3);
		listaP.add(p4);
		listaP.add(p5);
		
		return listaP;
	}
	
}
